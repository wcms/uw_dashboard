<?php

namespace Drupal\uw_dashboard\Handler;

use Drupal\webform\WebformEntityListBuilder;

/**
 * Class UWWebformEntityListBuilder.
 *
 * Extends functionality by updating one method.
 */
class UWWebformEntityListBuilder extends WebformEntityListBuilder {

  /**
   * New function that modifies output of parent render.
   *
   * Only usage is on uw_dashboard, when webform filter autocomplete
   * matches one webform. This update will remove webform id, and
   * use filter to display one result, which creates redirect otherwise.
   */
  public function uwRender() {
    if ($this->keys && preg_match('#\(([^)]+)\)$#', $this->keys, $match) && !empty($match[0])) {
      $this->keys = trim(preg_replace('#\(([^)]+)\)$#', '', $this->keys));
    }

    return $this->render();
  }

}
