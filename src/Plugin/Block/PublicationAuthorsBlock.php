<?php

namespace Drupal\uw_dashboard\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Publication reference authors' block.
 *
 * @Block(
 *  id = "uw_publication_authors_block",
 *  admin_label = @Translation("Publication reference authors"),
 * )
 */
class PublicationAuthorsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager from the core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('current_user')
    );
  }

  /**
   * ContentManagementMenuBlock constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   The current user.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager,
    AccountProxyInterface $currentUser
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Add a message if the user does not have access to the block.
    if (!$this->currentUser->hasPermission('edit bibcite_contributor')) {
      return [
        '#markup' => 'You do not have permission to view this block.',
      ];
    }

    // Load the view.
    /** @var \Drupal\views\ViewExecutable $view */
    $view = $this->entityTypeManager
      ->getStorage('view')
      ->load('uw_view_pub_authors')
      ->getExecutable();

    // Set the display.
    $view->setDisplay('pub_reference_authors');

    // Execute the view.
    $view->execute();

    // Get the title.
    $title = $view->getTitle();

    // Get the rendered title for the view.
    $the_title_render_array = [
      '#markup' => $this->t('@title', ['@title' => $title]),
      '#allowed_tags' => ['h2'],
    ];

    // Display using the view template so ajax works properly.
    return [
      '#type' => 'view',
      '#name' => $the_title_render_array,
      '#view' => $view,
      '#display_id' => 'pub_reference_authors',
      '#embed' => TRUE,
      '#cache' => $view->getCacheTags(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // Get the parent form.
    $form = parent::blockForm($form, $form_state);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'edit bibcite_contributor');
  }

}
