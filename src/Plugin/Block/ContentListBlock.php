<?php

namespace Drupal\uw_dashboard\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\views\Views;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'ContentList' block.
 *
 * @Block(
 *  id = "uw_cbl_content_list",
 *  admin_label = @Translation("List content"),
 * )
 */
class ContentListBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * Entity type manager from the core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * ContentManagementMenuBlock constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Session\AccountProxyInterface $currentUser
   *   Current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    AccountProxyInterface $currentUser,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $currentUser;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // The display id of the view to be used.
    $display_id = 'content_list';

    // Get the view by machine id.
    $view = Views::getView('uw_view_content_list');

    // Set the display machine id.
    $view->setDisplay($display_id);

    // Add the content type restriction to the arguments.
    $arguments[] = $this->configuration['content_type'];

    // Check if we have only certain content type filter, and if so remove it
    // from the exposed filters list.
    if (isset($this->configuration['content_type']) && $this->configuration['content_type'] !== 'all') {

      // Remove the content type filter from the view.
      $view->removeHandler($view->current_display, 'filter', 'type');
    }

    // Process Default moderation state filter field.
    if (isset($this->configuration['moderation_state'])) {

      // Get moderation state.
      $state = $this->configuration['moderation_state'];

      // Get current handler with old value.
      $moderation_state = $view->getHandler($display_id, 'filter', 'current_state_views_filter');

      // Set moderation_state configuration value.
      $moderation_state['value'] = [$state => $state];

      // Set handler with new value.
      $view->setHandler($display_id, 'filter', 'current_state_views_filter', $moderation_state);

    }

    // Check if we have an author restriction, if so add to arguments and
    // remove from exposed filters.
    if (isset($this->configuration['display_my_content']) && $this->configuration['display_my_content']) {

      // Add the current user to the arguments list.
      $arguments[] = $this->currentUser->id();

      // Remove the author and last updated by exposed filter,
      // since we are only displaying the users content.
      $view->removeHandler($view->current_display, 'filter', 'uid');
      $view->removeHandler($view->current_display, 'filter', 'revision_uid');
    }

    // Set the arguments that come from config.
    $view->setArguments($arguments);

    // Remove the changed by sort order.
    $view->removeHandler($view->current_display, 'sort', 'changed');
    $view->removeHandler($view->current_display, 'sort', 'created');

    // If there is a sort order set and it is not the changed by, add it.
    if (isset($this->configuration['sort_by']) && $this->configuration['sort_by'] !== 'changed') {
      $view->addHandler(
        $view->current_display,
        'sort',
        'node_field_data',
        $this->configuration['sort_by'],
        ['order' => $this->configuration['sort_order']]
      );
    }

    // Reset the changed by order.
    if (isset($this->configuration['sort_by']) && $this->configuration['sort_by'] == 'changed') {
      $view->addHandler(
        $view->current_display,
        'sort',
        'node_field_data',
        $this->configuration['sort_by'],
        ['order' => $this->configuration['sort_order']]
      );
    }
    else {
      $view->addHandler(
        $view->current_display,
        'sort',
        'node_field_data',
        'changed',
        ['order' => 'asc']
      );
    }

    // Execute the view.
    $view->execute();

    // Get the title.
    $title = $view->getTitle();

    // Get the rendered title for the view.
    $the_title_render_array = [
      '#markup' => $this->t('@title', ['@title' => $title]),
      '#allowed_tags' => ['h2'],
    ];

    // Display using the view template so ajax works properly.
    return [
      '#type' => 'view',
      '#name' => $the_title_render_array,
      '#view' => $view,
      '#display_id' => $display_id,
      '#embed' => TRUE,
      '#cache' => $view->getCacheTags(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // Get the node types.
    $node_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();

    // Set the all options for options array.
    $options = [
      'all' => 'All',
    ];

    // Step through each of the node types and add to options array.
    foreach ($node_types as $node_type) {
      $options[$node_type->id()] = $node_type->label();
    }

    // Fieldset for filters.
    $form['filters'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Filters'),
    ];

    // Set the content type form element.
    $form['filters']['content_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Content type'),
      '#options' => $options,
      '#default_value' => isset($this->configuration['content_type']) ? $this->configuration['content_type'] : 'all',
    ];

    // Set the content type form element.
    $form['filters']['moderation_state'] = [
      '#type' => 'select',
      '#title' => $this->t('Default moderation state filter'),
      '#options' => [
        'All' => $this->t('- Any -'),
        'uw_workflow-uw_wf_archived' => $this->t('Archived'),
        'uw_workflow-draft' => $this->t('Draft'),
        'uw_workflow-uw_wf_needs_review' => $this->t('Needs Review'),
        'uw_workflow-published' => $this->t('Published'),
      ],
      '#default_value' => isset($this->configuration['moderation_state']) ? $this->configuration['moderation_state'] : 'All',
    ];

    // Set the display only my content form element.
    $form['filters']['display_my_content'] = [
      '#type' => 'select',
      '#title' => $this->t('Display my content only'),
      '#options' => ['No', 'Yes'],
      '#default_value' => isset($this->configuration['display_my_content']) ? $this->configuration['display_my_content'] : 'no',
    ];

    // Fieldset for sorting.
    $form['sort'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Sorting'),
    ];

    // Set the sort by form element.
    $form['sort']['sort_by'] = [
      '#type' => 'select',
      '#title' => $this->t('Sort by'),
      '#options' => [
        'title' => $this->t('Title'),
        'type' => $this->t('Content type'),
        'uid' => $this->t('Author'),
        'revision_uid' => $this->t('Last update by'),
        'changed' => $this->t('Last updated'),
      ],
      '#default_value' => isset($this->configuration['sort_by']) ? $this->configuration['sort_by'] : 'last_updated',
    ];

    // Set the sort order form element.
    $form['sort']['sort_order'] = [
      '#type' => 'select',
      '#title' => $this->t('Sort order'),
      '#options' => [
        'asc' => $this->t('Ascending'),
        'desc' => $this->t('Descending'),
      ],
      '#default_value' => isset($this->configuration['sort_order']) ? $this->configuration['sort_order'] : 'asc',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    $values = $form_state->getValues();
    // Set the config for content type.
    $this->configuration['content_type'] = $values['filters']['content_type'];

    // Set the config for moderation state.
    $this->configuration['moderation_state'] = $values['filters']['moderation_state'];

    // Set the config for displaying only my content.
    $this->configuration['display_my_content'] = $values['filters']['display_my_content'];

    // Set the config for sort by.
    $this->configuration['sort_by'] = $values['sort']['sort_by'];

    // Set the config for sort order.
    $this->configuration['sort_order'] = $values['sort']['sort_order'];
  }

}
