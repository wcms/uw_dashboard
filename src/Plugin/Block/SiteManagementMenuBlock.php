<?php

namespace Drupal\uw_dashboard\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UwDblSiteManagement.
 *
 * @package Drupal\uw_site_management\Plugin\Block
 *
 * @Block(
 *   id = "uw_site_management_menu_block",
 *   admin_label = "Site management menu",
 * )
 */
class SiteManagementMenuBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The menu link tree.
   *
   * @var \Drupal\Core\Menu\MenuLinkTree
   */
  protected $menuLinkTree;

  /**
   * {@inheritdoc}
   */
  public function build() {
    $links = [];
    $menus = $this->getMenuItems(TRUE);

    foreach ($menus as $menu_item) {
      $links[] = [
        'title' => $menu_item->getTitle(),
        'url' => $menu_item->getUrlObject(),
      ];
    }

    return [
      '#theme' => 'links',
      '#links' => $links,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('menu.link_tree')
    );
  }

  /**
   * UwDblSiteManagement constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Menu\MenuLinkTreeInterface $menuLinkTree
   *   The menu link tree.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    MenuLinkTreeInterface $menuLinkTree
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->menuLinkTree = $menuLinkTree;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $menu_items = $this->getMenuItems();
    $config_menu = unserialize($this->configuration['selected_menus']);

    if ($config_menu) {
      $default_values = array_keys(array_filter($config_menu + $menu_items));
    }
    else {
      $default_values = array_keys($menu_items);
    }

    $form['selected_menus'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Menus to display'),
      '#options' => $menu_items,
      '#default_value' => $default_values,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    $menus = $form_state->getValue('selected_menus');
    // Storing as serialized to know which menu item is unchecked (not to
    // display) and which menu is added after this block has been configured.
    $this->configuration['selected_menus'] = serialize($menus);

    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Loads menu items with permissions taken into account.
   *
   * @param bool $configured
   *   Load only configured if TRUE. Otherwise returns all menu items.
   *
   * @return array
   *   List of menu items user has access to.
   */
  protected function getMenuItems(bool $configured = FALSE): array {
    $result_menu = [];
    // Load menu tree with children items.
    $menu = $this->menuLinkTree->load('uw-menu-site-management', new MenuTreeParameters());

    // Run trough transform, to append access and sort.
    $tree = $this->menuLinkTree->transform($menu, [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ]);

    foreach ($tree as $menu_item) {
      if ($configured) {
        $configured_menu = unserialize($this->configuration['selected_menus']);
        // Check if menu item is configured to be displayed and check if user
        // has permission to open menu link destination.
        if ($menu_item->access->isAllowed()) {
          if (in_array($menu_item->link->getPluginId(), array_filter($configured_menu)) ||
            !in_array($menu_item->link->getPluginId(), array_keys($configured_menu))) {
            $result_menu[$menu_item->link->getPluginId()] = $menu_item->link;
          }
        }
      }
      else {
        // Check if user has permissions to navigate to menu item destination.
        if ($menu_item->access->isAllowed()) {
          $result_menu[$menu_item->link->getPluginId()] = $menu_item->link->getTitle();
        }
      }
    }

    return $result_menu;
  }

}
