<?php

namespace Drupal\uw_dashboard\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\uw_cfg_common\Service\UWServiceInterface;

/**
 * Provides a 'ContentType' block.
 *
 * @Block(
 *  id = "uw_cbl_content_type_list",
 *  admin_label = @Translation("Content type use"),
 * )
 */
class ContentTypeBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager from the core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * UW service.
   *
   * @var \Drupal\uw_cfg_common\Service\UWServiceInterface
   */
  protected $uwService;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {

    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('uw_cfg_common.uw_service')
    );
  }

  /**
   * ContentTypeBlock constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param Drupal\uw_cfg_common\Service\UWServiceInterface $uwService
   *   UW Service.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager,
    UWServiceInterface $uwService
  ) {

    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
    $this->uwService = $uwService;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {

    // The build array.
    $cts = [];
    // Get the config for the block.
    $config = $this->configuration;

    if ($config['content_type_choice'] == 'all') {

      // Load all the content types.
      $content_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();

      // Step through all the content types and setup array of
      // content types.
      foreach ($content_types as $content_type) {
        $cts[] = $content_type->get('type');
      }
    }
    else {

      // Get the list of specific content types from the config.
      $cts = $this->configuration['specific_content_types'];
    }

    // Get array using UW service to ge the list of content types.
    $list = $this->uwService->getContentTypeUsage($cts);

    // If there is no content in the content type selected,
    // replace the list with a message that can be displayed.
    if (count($list) == 0) {
      $list = [
        [
          [
            'data' => $this->t('There is no content in the selected content types.'),
            'colspan' => 4,
          ],
        ],
      ];
    }

    // Set up the build array using a table as theming.
    return [
      '#theme' => 'table',
      '#header' => [
        $this->t('Content type'),
        $this->t('Total'),
        $this->t('Published'),
        $this->t('Unpublished'),
      ],
      '#rows' => $list,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // A fieldset to hold the content types selections.
    $form['content'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Content type usage'),
    ];

    // Select list for all content types or specific ones.
    $form['content']['content_type_choice'] = [
      '#type' => 'select',
      '#title' => $this->t('Select content types to be displayed'),
      '#options' => [
        'all' => $this->t('All'),
        'specific' => $this->t('Specific'),
      ],
      '#default_value' => $this->configuration['content_type_choice'] ?? 'all',
    ];

    // Array to hold the content types.
    $types = [];

    // Get the content types from the system.
    $content_types = $this->entityTypeManager->getStorage('node_type')->loadMultiple();

    // Put the content types in the types array.
    foreach ($content_types as $content_type) {
      $types[$content_type->id()] = $content_type->label();
    }

    // Fieldset to get the specific content type selections.
    // Note that we are faking the "required" to work with multiple checkboxes.
    // Per Eric, this is needed until a future Drupal release fixes the issue.
    $form['content']['specific'] = [
      '#type' => 'fieldset',
      '#title' => '<span class="js-form-required form-required">' . $this->t('Specific content types to be displayed') . '</span>',
      '#states' => [
        'visible' => [
          ':input[name="settings[content][content_type_choice]"]' => ['value' => 'specific'],
        ],
      ],
    ];

    // Select list of all the content types.
    $form['content']['specific']['content_types_displayed'] = [
      '#type' => 'checkboxes',
      '#options' => $types,
      '#default_value' => $this->configuration['specific_content_types'] ?? [],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {

    // Get the values from the form state.
    $values = $form_state->getValues();

    // If we are using specific content types, ensure that
    // there is at least one checked.
    if ($values['content']['content_type_choice'] == 'specific') {

      // Flag for no content type selected.
      $no_ct_flag = TRUE;

      // Step through all the content type checkbox and if one
      // is selected, set the flag and exit loop.
      foreach ($values['content']['specific']['content_types_displayed'] as $ct) {

        // If the checkbox is checked, set flag and exit loop.
        if ($ct) {
          $no_ct_flag = FALSE;
          break;
        }
      }

      // If the no content type flag is set, set the form error.
      if ($no_ct_flag) {
        $form_state->setError($form['content']['specific'], $this->t('You must select at least one content type.'));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    // Get the values from the form state.
    $values = $form_state->getValues();

    // Set the default of specific content types to blank array,
    // so that we do not get a warning about empty arrays.
    $this->configuration['specific_content_types'] = [];

    // Set the content type choice.
    $this->configuration['content_type_choice'] = $values['content']['content_type_choice'];

    // Set the chosen content type values return the values.
    if ($values['content']['content_type_choice'] == 'specific') {

      // Step through each of the specific content types and get the values.
      foreach ($values['content']['specific']['content_types_displayed'] as $ct) {
        if ($ct !== 0 && $ct !== NULL) {
          $ctd[] = $ct;
        }
      }

      // Set the specific content types.
      $this->configuration['specific_content_types'] = $ctd;
    }
  }

}
