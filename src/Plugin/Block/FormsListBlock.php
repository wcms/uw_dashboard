<?php

namespace Drupal\uw_dashboard\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'WebformsList' block.
 *
 * @Block(
 *  id = "uw_cbl_forms_list",
 *  admin_label = @Translation("List forms"),
 * )
 */
class FormsListBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Entity type manager from the core.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager')
    );
  }

  /**
   * ContentManagementMenuBlock constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entityTypeManager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // The build array.
    $build = [];

    // Get the block config.
    $config = $this->configuration;

    // Get entity list builder using entity type manager.
    $entity_type_list = $this->entityTypeManager
      ->getListBuilder('webform');

    // If there is a category, then set the category
    // for the list builder, so that it defaults to
    // the selected category in block config.
    // If there is no category, just get the render
    // array.
    if (
      isset($config['category']) &&
      $config['category'] !== 'all'
    ) {

      // If this already an array, we can just pass it along.
      // If it is not an array, we need to make it one.
      if (is_array($config['category'])) {
        $entity_type_list->setCategory($config['category']);
      }
      else {
        $entity_type_list->setCategory($config['category']);
      }

      // Get the render array from the entity list.
      $render = $entity_type_list->uwRender();

      // Remove the category filer.
      unset($render['filter_form']['filter']['category']);
    }
    else {
      $render = $entity_type_list->uwRender();
    }

    // Render the entity list.
    $build['entity_list'] = $render;

    // Get the total number of results.  There was no easy way
    // to get this value from the render array, either using the
    // translatable markup, of the entity list builder.  The only
    // way was the get the actual string and pick off up to the
    // first space.
    if (
      count($build['entity_list']['info']) > 0 &&
      isset($build['entity_list']['info']['#markup'])
    ) {
      $total_num_of_results = strtok(
        $build['entity_list']['info']['#markup']->__toString(),
        ' '
      );
    }
    else {
      $total_num_of_results = 0;
    }

    // If the total number of results is greater than 50, then add
    // a link to view all the webforms to the info part of the
    // render array.
    if ($total_num_of_results > 50) {
      $build['entity_list']['info']['#markup'] .= $this->t('<br><a href="../admin/structure/webform">View all webforms</a>');
      $build['entity_list']['more_info']['#markup'] = $this->t('<p class="pager__items"><a href="../admin/structure/webform?page=1">Next ›</a></p>');
    }

    // Remove the pager from the webform list.
    unset($build['entity_list']['pager']);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {

    // Get the category.
    $categories = $this->entityTypeManager
      ->getStorage('webform')
      ->getCategories(FALSE);

    // Set the all options for options array.
    $options = [
      'all' => 'All',
    ];

    // Step through each of the node types and add to options array.
    foreach ($categories as $category) {
      $options[$category] = $category;
    }

    // Fieldset for filters.
    $form['filters'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Filters'),
    ];

    // Set the content type form element.
    $form['filters']['category'] = [
      '#type' => 'select',
      '#title' => $this->t('Categories to display'),
      '#options' => $options,
      '#default_value' => $this->configuration['category'] ?? 'all',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {

    $values = $form_state->getValues();

    // Set the config for category.
    $this->configuration['category'] = $values['filters']['category'];

    // Set the config for displaying only my forms.
    $this->configuration['display_my_forms'] = $values['filters']['display_my_forms'];

    // Set the config for sort by.
    $this->configuration['sort_by'] = $values['sort']['sort_by'];

    // Set the config for sort order.
    $this->configuration['sort_order'] = $values['sort']['sort_order'];
  }

}
