document.addEventListener('click', function (event) {
  const detailsElements = document.querySelectorAll('details[name="uw-roles"]');
  detailsElements.forEach(details => {
    if (!details.contains(event.target)) {
      details.removeAttribute('open');
    }
  });
});
